package cz.pospech.ataccama.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * @author <a href="mailto:Michal.Pospisek@embedit.cz">Michal Pospisek</a> on 17.05.2021
 */
@Entity
class Connection(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long? = null,
    val username: String,
    val password: String,
    val name: String,
    val hostname: String,
    val port: Int,
    val databaseName: String
)