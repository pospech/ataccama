package cz.pospech.ataccama

import cz.pospech.ataccama.domain.Connection
import cz.pospech.ataccama.model.ConnectionDetail
import org.springframework.core.convert.converter.Converter

/**
 * @author <a href="mailto:Michal.Pospisek@embedit.cz">Michal Pospisek</a> on 17.05.2021
 */
class ConnectionConverter : Converter<ConnectionDetail, Connection> {
    override fun convert(source: ConnectionDetail): Connection?
        = Connection(
            username = source.username!!,
            password = source.password!!,
            name = source.name!!,
            hostname = source.hostname!!,
            port = source.port!!,
            databaseName = source.databaseName!!
        )
}