package cz.pospech.ataccama

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AtaccamaApplication

fun main(args: Array<String>) {
	runApplication<AtaccamaApplication>(*args)
}
