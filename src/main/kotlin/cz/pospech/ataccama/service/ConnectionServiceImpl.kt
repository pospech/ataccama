package cz.pospech.ataccama.service

import cz.pospech.ataccama.dao.ConnectionRepository
import cz.pospech.ataccama.model.Connection
import cz.pospech.ataccama.model.ConnectionDetail
import cz.pospech.ataccama.rest.controller.ConnectionsControllerService
import org.springframework.core.convert.converter.Converter
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

/**
 * @author <a href="mailto:Michal.Pospisek@embedit.cz">Michal Pospisek</a> on 17.05.2021
 */
@Service
class ConnectionServiceImpl(
    val repository: CrudRepository<cz.pospech.ataccama.domain.Connection, Long>
    val converter: Converter<ConnectionDetail, cz.pospech.ataccama.domain.Connection>
) : ConnectionsControllerService {

    override fun createConnection(name: String, connectionDetail: ConnectionDetail?): ConnectionDetail {
        if(connectionDetail == null) {

            return
        }

        converter.convert(connectionDetail)
        repository.save()
    }

    override fun deleteConnectionByName(name: String): ConnectionDetail {
        TODO("Not yet implemented")
    }

    override fun getConnectionByName(name: String): List<Connection> {
        TODO("Not yet implemented")
    }

    override fun getConnectionsList(): List<Connection> {
        TODO("Not yet implemented")
    }

    override fun updateConnectionByName(name: String, connectionDetail: ConnectionDetail?): ConnectionDetail {
        TODO("Not yet implemented")
    }
}