package cz.pospech.ataccama.dao;

import org.springframework.stereotype.Repository;
import cz.pospech.ataccama.domain.Connection;

/**
 * @author <a href="mailto:Michal.Pospisek@embedit.cz">Michal Pospisek</a> on 17.05.2021
 */
@Repository
public interface ConnectionRepository : CrudRepository<Connection, Long> {
}
